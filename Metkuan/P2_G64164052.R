dt <- faithful
summary(dt)

model <- lm(formula = eruptions~waiting, data = dt)
summary(model)

#with(dt, plot(eruptions~waiting))

with(dt, cor(eruptions, waiting))
with(dt, cor(eruptions, waiting)^2)

#ketidaknormalan sisaan
resid <- rstandard(model)
qqnorm(resid, ylab = "Standar Residuals", xlab = "Normal Scores", main = "ERUPSI")
qqline(resid)

#Ketidakhomogenan ragam
sisaan <- resid(model)
plot(dt$eruptions, sisaan, ylab = "Residuals", xlab = "Eruptions", main = "Plot Sisaan")
abline(0,0)

#Autocorrelation
nrow(dt)
plot(1:272, sisaan, ylab = "Residuals", xlab = "Case", main = "Plot Sisaan dengan Nomor Data")