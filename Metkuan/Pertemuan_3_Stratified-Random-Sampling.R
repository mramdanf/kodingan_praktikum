#SOAL LATIHAN

#diketahui
N1 = 155
N2 = 62
N3 = 93
Ragam1 = 25
Ragam2 = 225
Ragam3 = 100
a1 = 1/3
a2 = 1/3
a3 = 1/3
B = 2

#Ditanyakan : Berapa banyak sample yang diambil dari setiap strata

#Jawab

#Cari Sigma Ni^2*Ragam^2

pembilang <- (((N1)^2 * Ragam1)/a1) + (((N2)^2 * Ragam2)/a2) + (((N3)^2 * Ragam3)/a3)
pembilang

#Cari sigma Ni*Ragam^2(penyebut1) & N^2 * D(penyebut2)

penyebut1 <- (((N1) * Ragam1)) + (((N2) * Ragam2)) + (((N3) * Ragam3))
penyebut1
penyebut2 <- (N1 + N2 + N3)^2 * (B^2 / 4)
penyebut2

#Cari n sample total

n <- pembilang/(penyebut1 + penyebut2)
n

#Cari n sample untuk masing-masing strata
n1 <- n*a1
n2 <- n*a2
n3 <- n*a3
n1
n2
n3