#s.id/x8Y

#Soal 1
#Sort data terlebih dahulu lalu dipotong N1 = 14, N2 = 14, N3 = 15
#Ambil sample dari masing - masing strata n1=6, n2=6, n3=7
# 1. Hitung dugaan total populasi
# 2. Dugaan ragam total populasi
# 3. Dugaan error total populasi

#kumpulin R nya saja, kasih komen Nama sama NIM

#========== Soal 1 ===========
populasi=c(40000,60000,70000,65000,100000,100000,70000,100000,25000,120000,100000,70000,15000,50000,300000,20000,80000,122000,50000,100000,50000,90000,55000,66000,150000,100000,50000,55000,100000,80000,80000,50000,50000,100000,100000,45000,60000,80000,60000,70000,100000,40000,50000)
populasi=sort(populasi)

N1=14
N2=14
N3=15
n1=6
n2=6
n3=7

data1=populasi[1:14]
data2=populasi[15:28]
data3=populasi[29:43]


id_s_n1=sample(1:N1, n1, replace = FALSE)
id_s_n2=sample(1:N2, n2, replace = FALSE)
id_s_n3=sample(1:N3, n3, replace = FALSE)

sample1=data1[id_s_n1]
sample2=data2[id_s_n2]
sample3=data3[id_s_n3]

#------Dugaan total populasi-------
#Dugaan total populasi - sample 1
rata_s1<-mean(sample1)
total_s1=rata_s1*N1

#Dugaan total populasi - sample 2
rata_s2<-mean(sample2)
total_s2=rata_s2*N2

#Dugaan total populasi - sample 3
rata_s3<-mean(sample3)
total_s3=rata_s3*N2

#Dugaan mean populasi
mean_populasi=(total_s1+total_s2+total_s3)/(N1+N2+N3)


total_populasi=(N1+N2+N3)*mean_populasi
#------End Dugaan total populasi-------

#------Dugaan ragam total populasi-------

#Sample 1
#Dugaan ragam total populasi
h1=0
for(i in 1:n1) {
  
  # Sigma (yi-ybar)^2
  h1=h1+((sample1[i]-rata_s1)^2)
}

#s^2
ragam_sample1=h1/(n1-1)

#Dugaan ragam total populasi
ragam_total_populasi1 = ( (N1^2) * (1-(n1/N1)) * (ragam_sample1/n1) )


#Sample 2
#Dugaan ragam total populasi
h2=0
for(j in 1:n2){
  h2=h2+((sample2[j]-rata_s2)^2)
}

ragam_sample2=h2/(n2-1)

#Dugaan ragam total populasi
ragam_total_populasi2 = ( (N2^2) * (1-(n2/N2)) * (ragam_sample2/n2) )

#Sample 3
#Dugaan ragam total populasi
h3=0
for(k in 1:n3){
  h3=h3+((sample3[k]-rata_s3)^2)
}

ragam_sample3=h3/(n1-1)

#Dugaan ragam total populasi
ragam_total_populasi3 = ( (N3^2) * (1-(n3/N3)) * (ragam_sample3/n3) )

ragam_total_populasi=ragam_total_populasi1+ragam_total_populasi2+ragam_total_populasi3
#------End Dugaan ragam total populasi-------

#------Dugaan error total populasi-------
error_plus_min=2*sqrt(ragam_total_populasi);

#------End Dugaan error total populasi-------

hasil_soal1=list(
  dugaan_total_populasi=total_populasi,
  dugaan_ragam_total_populasi=ragam_total_populasi,
  dugaan_error_total_populasi_plus_min=error_plus_min
)
hasil_soal1


#========== Soal 2 ===========
# cost sample 1 = 9
# cost sample 2 = 9
# cost sample 3 = 16
# 
# jumlah data 1 = 155
# jumlah data 2 = 62
# jumlah data 3 = 93
# 
# ragam 1 = 25
# ragam 2 = 225
# ragam 3 = 100

#B = 2 #gunakan D rataan
#Ditanyakan
#Cari n1, n2, n3
NN1=155
NN2=62
NN3=93
var1=25
var2=255
var3=100
c1=9
c2=9
c3=16
B=2
D=(B^2)/4
u=(((NN1)*var1)/sqrt(c1)) + (((NN2)*var2)/sqrt(c2)) + (((NN3)*var3)/sqrt(c3))
p=(NN1*var1*sqrt(c1)) + (NN2*var2*sqrt(c2)) + (NN3*var3*sqrt(c3))
w=(NN1*var1) + (NN2*var2) + (NN3*var3)
n=u*p / (((NN1+NN2+NN3)^2)*D) + w
n1= ceiling(n*(((NN1*var1)/sqrt(c1))/u))
n2= ceiling(n*(((NN2*var2)/sqrt(c2))/u))
n3= ceiling(n*(((NN3*var3)/sqrt(c3))/u))

hasil_soal2=list(
  n=n,
  n1=n1,
  n2=n2,
  n3=n3
)
hasil_soal2
