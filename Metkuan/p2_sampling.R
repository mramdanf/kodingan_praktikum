#Simple random sampling

#Soal 1
# hitung:
#   1. error sample
#   2. error populasi
  
N<-1000 #populasi
n<-200 #sample

rata_n<-94.22
ragam_n<-415.21

#Dugaan ragam rata2 populasi
v<-(ragam_n/n)*((N-n)/N)

#Error dugaan ragam rata2 populasi
error_n<-2*sqrt(v)
error_n

#Dugaan error total populasi
error_N<-2*sqrt(v*N)
error_N

#----------------------------------------------#

#Tugas di tempat
#Soal 2
# Hitung:
#   1. rata2 sample
#   2. dugaan total populasi
#   3. error rata - rata populasi
#   4. error total populasi
#   5. masukan ke dalam list dengan nama: statistik
#      rata2_populasi, total_populasi, varian_sample,
#      error_sample, error_populasi
     
P<-60
s<-10

dataku<-c(300,325,315,310,330,280,360,380,340,350)

rata_s<-mean(dataku)
rata_s

total_P=rata_s*P
total_P

h<-0
for(i in 1 : s) {
  h<-h+((dataku[i]-rata_s)*(dataku[i]-rata_s))
}

var_s<-h/(s-1)
var_s

#error pada rata populasi
v<-(var_s/s)*((P-s)/P)
error_s<-2*sqrt(v)
error_s

#error total pada populasi
# error_P<-2*sqrt(v*P)
# error_P

error_P=2*sqrt(P*P*(1-(s/P))*(var_s/s))

Informasi_statistika<-list(
  rataan_populasi=rata_s,
  total_populasi=total_P,
  varian_sample=var_s,
  error_sample=error_s,
  error_populasi=error_P
)
Informasi_statistika

# Operasi dari list
# > Informasi_statistika$rataan_populasi
# [1] 329
# > Informasi_statistika$rataan_populasi + 1
# [1] 330


#----------------------------------------------#

#Bangkit data dan sampling
#replacement=FALSE // angka yang muncul beda2
#replacement=TRUE // bisa muncul angka yang sama
populasi=sample(1:200, 100, replace=FALSE)
populasi

id_sample=sample(1:100, 20, replace = FALSE)
id_sample
sample=populasi[id_sample]
sample








