# Tugas : Stratified random sampling
# 
# 1.
# -Bangkitkan data dummy sebanyak 500 data, dengan range 0-100 dan pengunlanan TRUE
# -Bagi data yang didapatkan kedalam 3 starta dengan syarat
#   *strata1 : range 0-50
#   *strata2 : range 51-75
#   *strata3 : range 76-100
# -a1=1/3, a2=1/3, a3=1/3
# -Hitung:
#   *dugaan rataan
#   *dugaan total
#   *error untuk rata - rata populasi
#   *error untuk total populasi
#   *hitung bond/range untuk masing - masing rataan dan populasi

#--------- Nomor 1 ----------#
myData <- sample(1:100, 500, replace = TRUE)
strata1 <- c()
strata2 <- c()
strata3 <- c()
for (i in 1 : length(myData)) {
  if (myData[i]<=50)
    strata1 <- c(strata1, myData[i])
  else if (myData[i]<=75)
    strata2 <- c(strata2, myData[i])
  else if (myData[i]<=100)
    strata3 <- c(strata3, myData[i])
}
N <- length(myData)
N1 <- length(strata1)
N2 <- length(strata2)
N3 <- length(strata3)
a1 <- 1/3
a2 <- 1/3
a3 <- 1/3
n1 <- ceiling(N1*a1)
n2 <- ceiling(N2*a2)
n3 <- ceiling(N3*a3)
id_s1 <- sample(1:N1, n1, replace = FALSE)
sample_s1 <- strata1[id_s1]
id_s2 <- sample(1:N2, n2, replace = FALSE)
sample_s2 <- strata2[id_s2]
id_s3 <- sample(1:N3, n3, replace = FALSE)
sample_s3 <- strata3[id_s3]

#Dugaan Rataan
rata_s1 <- mean(sample_s1)
n_s1 <- n1*rata_s1
rata_s2 <- mean(sample_s2)
n_s2 <- n2*rata_s2
rata_s3 <- mean(sample_s3)
n_s3 <- n3*rata_s3
jml_n <- n_s1+n_s2+n_s3
dr <- jml_n/N

#Ragam Dugaaan Rataan
h1<-0
for(i in 1 : n1) {
  h1 <- h1 + ((sample_s1[i]-rata_s1)^2)
}
var_s1 <- h1/(n1-1)
l1 <- (n1^2) * (1-(n1/N1)) * (var_s1/n1)

h2 <- 0
for(j in 1 : n2) {
  h2 <- h2 + ((sample_s2[j]-rata_s2)^2)
}
var_s2 <- h2/(n2-1)
l2 <- (n2^2) * (1-(n2/N2)) * (var_s2/n2)

h3 <- 0
for(k in 1 : n3) {
  h3 <- h3 + ((sample_s3[k]-rata_s3)^2)
}
var_s3 <- h3/(n3-1)
l3 <- (n3^2) * (1-(n3/N3)) * (var_s3/n3)
d_ragam_r = (l1+l2+l3)/(N^2)

#Error Dugaan Rataan
e_d_rataan = 2*(sqrt(d_ragam_r))

#Bound/Range Rataan
b_r_atas <- dr + e_d_rataan
b_r_bawah <- dr - e_d_rataan


#Dugaan Total
d_total <- N * dr

#Ragam Dugaan Total
r_d_total <- (N^2) * (d_ragam_r)

#Error Dugaan Total
e_d_total <- 2*sqrt(r_d_total)

#Bound/Range Total
b_t_atas <- d_total + e_d_total
b_t_bawah <- d_total - e_d_total

hasil_no_satu <- list(
  dugaan_rataan=dr,
  dugaan_total=d_total,
  error_rata_populasi=e_d_rataan,
  error_total_populasi=e_d_total,
  bound_rataan_atas=b_r_atas,
  bound_rataan_bawah=b_r_bawah,
  bound_totoal_atas=b_t_atas,
  bound_total_bawah=b_t_bawah
)
hasil_no_satu

# 2.
# Menggunakan data no 1
# cost1=3, cost2=4, cost3=5
# B=5
# hitung:
#   -Tentunkan jumlah sample yang dibutuhkan untuk masing - masing strata dengan cost dan
#    error yang sudah ditentukan

#--------- Nomor 2 ----------#
