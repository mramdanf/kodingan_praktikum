
#include <iostream>
#include <iomanip>>
#include <list>

using namespace std;

class Person {
   private:
      string nama;
      int usia;
      int tinggi;
      double berat;
      double imt;
      string status;
      double IMT() { double t=(double)tinggi/100.0; return (berat/(t*t)); }
      string StatusGizi() {
         double t=IMT();
         if (t<17.0) return "sangat kurus";
         else if (t<18.5) return "kurus";
         else if (t<25.0) return "normal";
         else if (t<28.0) return "gemuk";
         else return "sangat gemuk"; }
   public:
      Person() { nama=""; usia=tinggi=0; berat=0.0; }
      void setPerson(string nm, int u, int t, double b) {
         nama=nm; usia=u; tinggi=t; berat=b;
         imt=IMT(); status=StatusGizi();
      }
      string getNama() { return nama; }
      int getUsia() { return usia; }
      int getTinggi() { return tinggi; }
      double getBerat() { return berat; }
      double getIMT() { return imt; }
      string getStatusGizi() { return status; }
      void cetak() {
         cout << nama << " " << usia << " " << tinggi << " ";
         cout << fixed << setprecision(2) << berat << " " << imt << " " << status << endl;
      }
};
Person AddPerson()
{
    int usia, tinggi;
    double berat;
    string nama;
    cin>>nama>>usia>>tinggi>>berat;
    Person p;
    p.setPerson(nama, usia, tinggi, berat);
    return p;
}

int main()
{
    int n,x, i, usia, tinggi;
    double berat;
    string nama;
    list<Person> dt;
    list<Person>::iterator it;
    cin >> n;
    for (i = 0; i<n ; i++)
    {
        dt.push_back(AddPerson());
    }
    while(cin>>n, n!=-9)
    {
        switch(n)
        {
            case 1: dt.push_front(AddPerson()); break;
            case 2: dt.push_back(AddPerson()); break;
            case 3: dt.pop_front(); break;
            case 4: dt.pop_back(); break;
            case 5: cin >> x; it=dt.begin();for(i=0;i<x;i++) ++it; dt.insert(it,AddPerson()); break;

        }
    }



   for(it=dt.begin(); it != dt.end(); ++it)
   {
        it->cetak();
    }
   return 0;
}







// Raden Asri Ramadhina Fitriani - G64154007
