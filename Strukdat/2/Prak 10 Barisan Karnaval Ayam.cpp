#include <iostream>
#include <iomanip>
#include <list>

using namespace std;

class Ayam {
   private:
      string warna;
      int nomor;
   public:
      Ayam(string wrn="", int no=0) {warna=wrn; nomor=no;}
      string getWarna(){return warna;}
      int getNomor(){return nomor;}
      void print() {
         cout << warna<<":"<<nomor << "->";
      }
};

int main()
{
    int n,i,nomor,ada=0;
    string warna;
    list<Ayam> dt;
    list<Ayam>::iterator it;
    cin >> n;
    for (i = 0; i<n ; i++)
    {
        cin>>warna>>nomor;
        Ayam a(warna,nomor); //create object ayam
        it=dt.begin();
        while(it!=dt.end())
        {
            if(it->getWarna()==warna) 
            {
				//digeser biar masuk di barisan terakhir dari sekelompok warna
				while(it->getWarna()==warna && it!=dt.end())
					++it;
                break;
            }
            ++it;
        }
			
        if(it==dt.end())
            dt.push_back(a);
        else
            dt.insert(it,a);
    }
   for(it=dt.begin(); it != dt.end(); ++it)
   {
        it->print();
    }
    cout<<"NULL"<<endl;
   return 0;
}







// Raden Asri Ramadhina Fitriani - G64154007
