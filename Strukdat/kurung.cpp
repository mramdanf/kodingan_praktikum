#include <iostream>
#include <string>
#include <stack>
using namespace std;


int main() {
    stack<char> dt;
    string str;

    cin >> str;
    for(int i=0;i<str.length();i++) {
        if(str[i]=='(' || str[i]=='{' || str[i]=='[' || str[i]=='<') {
            dt.push(str[i]);
        } else {
            if (dt.top()=='(' && str[i]==')') {
                dt.pop();
            } else if (dt.top()=='{' && str[i]=='}') {
                dt.pop();
            } else if (dt.top()=='[' && str[i]==']') {
                dt.pop();
            } else if (dt.top()=='<' && str[i]=='>') {
                dt.pop();
            }
        }
    }
    if (dt.empty()) cout << "VALID" << endl;
    else cout << "TIDAK VALID" << endl;
}
