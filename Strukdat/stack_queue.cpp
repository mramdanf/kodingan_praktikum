#include <iostream>
#include <list>
#include <string>
#include <stdio.h>
using namespace std;

typedef list<int> li;

class DLL {
    li::iterator it;
    li dt;

    public:
        void print() {
            it=dt.begin();
            while(it!=dt.end()) {
                cout << *it << endl;
                ++it;
            }
        }

        void pop_front() {
            if (!dt.empty()) {
                it = dt.begin();
                dt.erase(it);
            }
            
        }

        void pop_back() {
            if (!dt.empty()) {
                li::iterator pra;
                pra=it=dt.begin();

                while(it!=dt.end()) {
                    pra=it;
                    ++it;
                }
                dt.erase(pra);
            }
        }
        
        void push_back(int val) {
        	dt.push_back(val);
		}
		
		void push_front(int val) {
			dt.push_front(val);
		}
};

int main() {
    DLL dt;
    int n,t;
    string s;

    cin >> n;
    for(int i=0; i<n; i++) {
        cin >> s;
        if (s.compare("push_back") == 0) {
            cin >> t;
			dt.push_back(t); 
        } else if (s.compare("push_front") == 0) {
            cin >> t;
			dt.push_front(t); 
        } else if (s.compare("pop_front") == 0) {
            dt.pop_front();
        } else if (s.compare("pop_back") == 0) {
            dt.pop_back();
        }
    }
    dt.print();
    return 0;
}
