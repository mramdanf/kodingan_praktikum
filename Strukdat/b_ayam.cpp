#include <iostream>
#include <list>
#include <iomanip>
using namespace std;


list<float>::iterator pt;

class DLL : public list<float> {

	public:
        void print();
        void add(float in);
};

void DLL::add(float in) {
    list<float>::iterator ptr;

    ptr=this->begin();
    while(ptr!=this->end() && *ptr<in) {
        ++ptr;
    }

    if (ptr != this->end()) {
        this->insert(ptr, in);
    } else {
        this->push_back(in);
    }
}
void DLL::print() {
    list<float>::iterator ptr;
    cout << fixed << setprecision(1);
    for(ptr=this->begin(); ptr!=this->end(); ++ptr) {
        cout << *ptr << "->";
    }
    cout << "NULL" << endl;
}
int main() {
    DLL dt;
    int m,n;
    float in;

    cin >> n;
    while(n--) {
		cin >> in;
		dt.push_back(in);
	}

    cin >> m;
    while(m--) {
        cin >> in;
        dt.add(in);
    }

    dt.print();
}
