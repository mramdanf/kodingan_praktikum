#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

struct node {
     string data;
     struct node* left;
     struct node* right;
};

typedef struct node Node;

struct node* newNode(string data) {
     struct node* node = new (struct node);
     node->data = data;
     node->left = NULL;
     node->right = NULL;

     return(node);
}

struct node *searchPostorder(struct node* node, string nama)
{
     if (node == NULL || node->data.compare(nama) == 0) {
        return node;
     }

     searchPostorder(node->left, nama);
     searchPostorder(node->right, nama);

}


int main() {
    string in;
    struct node *hasil;
    struct node *root  = newNode("Ari");

    root->left         = newNode("Budi");
    root->right        = newNode("Cintia");

    root->left->left   = newNode("Desi");
    root->left->right  = newNode("Endang");

    root->right->left   = newNode("Fitrah");
    root->right->right  = newNode("Gani");

    root->left->left->left = newNode("Hesti");
    root->left->left->right = newNode("Ita");

    cin >> in;
    hasil = searchPostorder(root, in);
    if (hasil == NULL) {
        cout << "Nama tidak ditemukan." << endl;
    } else {
        if (hasil->left == NULL) {
            cout << "Tidak memiliki anak." << endl;
        } else {
            cout << hasil->left->data << endl;
            cout << hasil->right->data << endl;
        }
    }
    getchar();
    return 0;
}
