#include <iostream>
#include <stack>
#include <string>

using namespace std;

class myStack {
	stack<char> buka;
	stack<char> tutup;
	stack<char>	hasil;
	
	public:
		void push_data(char c) {
			if (c=='[') tutup.push(']');
			else if (c==']') {
				if (tutup.empty()) buka.push('[');
				else tutup.pop();
			}
		}
		
		void gabung(string input) {
			while(!tutup.empty()) {
				hasil.push(tutup.top());
				tutup.pop();
			}
			for (int i=(input.length()-1);i>=0;i--) {
				hasil.push(input[i]);
			}
			while(!buka.empty()) {
				hasil.push(buka.top());
				buka.pop();
			}
		}
		
		void print() {
			while(!hasil.empty()) {
				cout << hasil.top();
				hasil.pop();
			}
			cout << endl;
		}
};

int main() {
    int i;
    string input;
    myStack dt;
    
    cin >> input;
    for (i=0;i<input.length();i++) {
    	dt.push_data(input[i]);
    }
    dt.gabung(input);
    dt.print();   
}
