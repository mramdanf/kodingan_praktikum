#include <iostream>
#include <stack>
#include <queue>
#include <string>
using namespace std;

class myQueue {
    queue<int> Q;
    stack<int> s;
    int c;
    public:
        myQueue() {c=0;}
        void add(int x, int y) {
            c+=y;
            while(y--) {
                Q.push(x);
            }
            cout << c << endl;
        }
        void del(int y) {
            c-=y;
            cout << Q.front() << endl;
            while(y--) {
                Q.pop();
            }
        }
        void rev() {
            while(!Q.empty()) {
                s.push(Q.front());
                Q.pop();
            }
            while(!s.empty()) {
                Q.push(s.top());
                s.pop();
            }
        }
};

int main() {
    int n,x,y;
    string kode;
    myQueue Que;
    cin >> n;
    while(n--) {
        cin >> kode;
        if (kode=="add") {
            cin >> x >> y;
            Que.add(x,y);
        } else if (kode=="del") {
            cin >> y;
            Que.del(y);
        } else if (kode=="rev") {
            Que.rev();
        }
    }

    return 0;

}
