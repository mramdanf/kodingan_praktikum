#include <iostream>
#include <list>
using namespace std;

typedef list<char> li;
class myList {
    li dt;

    public:
        void rev() {
            dt.reverse();
        }

        void print() {
        	li::iterator it;
			it = dt.begin();
            while(it!=dt.end()) {
                cout << *it;
                ++it;
            }
            cout << endl;
        }

        void push(char c) {
            dt.push_back(c);
        }

};

int main() {
    int n;
    string in;
    char lastCmd;

    cin >> n;
    while(n--) {
        cin >> in;
        myList ms;
        for(int i=0; i<in.length(); i++) {
            if (in[i] == '+') {
            	i++;
                ms.push(in[i]);
            } else if (in[i] == '^') {
                ms.rev();
            } 
        }

        ms.print();
    }

    return 0;
}
