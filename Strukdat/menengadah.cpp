#include <iostream>
#include <list>
using namespace std;

typedef struct node {
	int index, info;
} Node;

typedef list<Node> li;
class myList {
	li dt;
	li::iterator it;
	int ind;
	
	public:
		myList() {
			ind=1;
		}
		void push_back(int val) {
			Node n;
			n.index=ind++;
			n.info=val;
			
			if (ind==1) {
				cout << val << endl;
			} else {
				li::iterator max;
				max=dt.begin();
				for(it=dt.begin();it!=dt.end();++it) {
					if ((*it).index>(*max).index && (*it).info>(*max).info) {
						max=it;
					}
				}
				if (n.index>(*max).index && n.info>(*max).info) cout << 0 << endl;
				else cout << (*max).info << endl;
			}
			
			dt.push_back(n);
		}
		
		void print() {
			for(it=dt.begin();it!=dt.end();++it) {
					cout << (*it).index << " " << (*it).info << endl;
				}
				cout << endl;
		}
};

int main() {
	myList dt;
	int n,val;
	
	cin >> n;
	for(int i=0;i<n;i++) {
		cin >> val;
		dt.push_back(val);
	}
	dt.print();
	
	
}
