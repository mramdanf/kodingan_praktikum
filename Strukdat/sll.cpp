#include <iostream>
#include <string>
using namespace std;

struct node {
    string nim;
    float nilai;
    struct node* next;
};
typedef struct node Node;

class SLL {
Node *head, *tail;

public:
    void make() { head=NULL; tail=NULL;}
    Node *make(string nim, float nilai);  // make a node
    int isEmpty() { return (head==NULL); }
    void push_back(string nim, float nilai);
    void push_front(string nim, float nilai);
    void push_after(string nim, float nilai, string after);
    Node *find(string nim);
    Node *find_before(string nim);
    void del(string nim);
    void print();
};
Node *SLL::make(string nim, float nilai) {
    Node *temp = new(Node);
    temp->nim=nim;
    temp->nilai=nilai;
    temp->next=NULL;
    return temp;
}
void SLL::push_back(string nim, float nilai) {
    Node *ptr=make(nim, nilai);
    if (isEmpty()) {
        head=ptr; tail=ptr;
    } else {
        tail->next=ptr; tail=ptr;
    }
}
void SLL::push_front(string nim, float nilai) {
    Node *ptr=make(nim, nilai);
    if (isEmpty()) {
        head=ptr; tail=ptr;
    } else {
        ptr->next=head; head=ptr;
    }
}
void SLL::print() {
    Node *ptr=head;
    for (; ptr!=NULL; ptr=ptr->next)
        cout << "(" << ptr->nim << "," << ptr->nilai << ")->";
    cout << "NULL" << endl;
}
Node *SLL::find(string nim) {
    Node* ptr=head;
    if (isEmpty()) return NULL;
    else {
        while (ptr->next!=NULL && ptr->nim!=nim) {
            ptr=ptr->next;
        }
        if (ptr->nim==nim) return ptr;
        else return NULL;
    }
}
void SLL::push_after(string nim, float nilai, string after) {
	Node *ptr = find(after);
	if (ptr!=NULL) {
		Node *temp = make(nim, nilai);
		temp->next = ptr->next;
		ptr->next = temp;
	}
}
Node *SLL::find_before(string nim) {
	Node *ptr=head;
	Node *pra=head;
	if (isEmpty()) return NULL;
	else {
		while (ptr->next!=NULL && ptr->nim!=nim) {
			pra=ptr; ptr=ptr->next;
		}
		if (ptr->nim==nim) return pra;
		else return NULL;
	}
}
void SLL::del(string nim) {
	Node *ptr = find(nim);
	if (ptr==head) head=head->next;
	else {
		ptr = find_before(nim);
		if (ptr!=NULL) {
			if (ptr->next==tail) tail=ptr;
			ptr->next=(ptr->next)->next;
		}
	}
}
int main() {
   SLL list;
   list.make();
   list.push_back("G64204100", 3.14);
   list.push_back("G64204050", 3.67);
   list.push_front("G6420075", 2.05);
   list.print();
   list.push_after("G6420035", 2.89, "G64204100");
   list.print(); list.del("G64204100");
   list.print();
   return 0;
}
