#include <iostream>
#include <string>
using namespace std;

struct node {
    string nim;
    float nilai;
    struct node* next;
};
typedef struct node Node;

class SLL {
	Node *head, *tail;

	public:
		void make() { head=NULL; tail=NULL;}
		Node *make(int val);  // make a node
		int isEmpty() { return (head==NULL); }
		void push_back(int val);
		void push_front(int val);
		void push_after(int val, int after);
		Node *find(int val);
		Node *find_before(int val);
		void del(int val);
		void print();
};

int main() {
	cout << "tst" ;
}
