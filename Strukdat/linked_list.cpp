#include <iostream>
using namespace std;

struct node {
   int nilai;
   struct node *next;
};

typedef struct node Node;

class SLL {
    Node *head, *tail;
    public:
        SLL() {head=tail=NULL;}

        bool empty() {return head==NULL;}

        Node *make(int nilai) {
            Node *temp = new(Node);
            temp->next = NULL;
            temp->nilai = nilai;
            return temp;
        }

        void push_back(int nilai) {
            Node *temp = make(nilai);
            if (empty()) head=tail=temp;
            else {
                tail->next = temp;
                tail = temp;
            }
        }
        void push_front(int nilai) {
            Node *temp = make(nilai);
            if (empty()) head=tail=temp;
            else {
                temp->next = head;
                head = temp;
            }
        }
        void insert_at(int p, int v){
            if(empty()){
                push_back(v);
            }else{
                if(p<size()){
                    Node *av=new (Node);
                    Node *ptr= make(v);
                    int i=0;
                    for(av=head;av!=NULL;av=av->next){
                        if(p==0){
                            push_front(v);
                            break;
                        }else if(p-1==i){
                            ptr->next=av->next;
                            av->next=ptr;
                            break;
                        }
                        i++;
                    }
                }else{
                    push_back(v);
                }
            }
        }
        void hapus_depan() {
            if (!empty()) {
                head=head->next;
            }
        }
        void hapus_ke(int p) {
            if(empty()!=true || p<size()){
                Node *av=new (Node);
                Node *ptr=new (Node);
                int i=0;
                ptr=head;
                for(av=head;av!=NULL;av=av->next){
                    if(p==0){
                        hapus_depan();
                        break;
                    }else if(p==i){
                        ptr->next=av->next;
                        break;
                    }
                    ptr=av;
                    i++;
                }
            }
        }
        void print() {
            Node *ptr=new Node;

            if(empty()){
                cout << "empty" << endl;
            }else{
                for(ptr=head;ptr!=NULL;ptr=ptr->next){
                    cout << ptr->nilai;
                    if(ptr->next==NULL){
                        cout << endl;
                    }else{
                        cout << " ";
                    }
                }
            }
        }
        int size() {
            Node *ptr=new (Node);
            int i=0;
            for(ptr=head;ptr!=NULL;ptr=ptr->next){
                i+=1;
            }
            return i;
        }
};

int main() {
    SLL dt;
    char a;
    int v,p;
    cin >> a;
    while(a!='q') {
        if (a=='f') {
            cin >> v;
            dt.push_front(v);
        } else if (a=='i') {
            cin >> p >> v;
            dt.insert_at(p,v);
        } else if (a=='r') {
            dt.hapus_depan();
        } else if (a=='d') {
            cin >> p;
            dt.hapus_ke(p);
        }
        dt.print();
        cin >> a;
    }
    return 0;
}
