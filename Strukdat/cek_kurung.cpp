#include <iostream>
#include <stack>
#include <string>

using namespace std;

int cek_kurung(char a, char b){
    if (a == '[' && b == ']') return true;
    else if (a == '{' && b == '}') return true;
    else if (a == '(' && b == ')') return true;
    else if (a == '<' && b == '>') return true;
    else return false;

}
int main(){
    stack<char> dt;

    int flag=0;
    string input;
    cin>> input;

    for (int i=0;i<input.length();i++){

        if (input[i]=='('||input[i]== '[' || input[i] == '{' || input[i]=='<'){
                dt.push(input[i]);
        }else{
            if(dt.empty()){
                flag=1;
                break;
            }
            if(cek_kurung(dt.top(), input[i])){
                dt.pop();
            } else {
                flag=1;
                break;
            }
        }

    }
	
	if (dt.empty() && !flag) cout << "VALID" << endl;
	else cout << "TIDAK VALID" << endl;

}
