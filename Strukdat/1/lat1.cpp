#include<iostream>
#include<set>
using namespace std;

int main(){
    set<int> mySet;
    set<int>::iterator it;
    int input;

    cin >> input;

    while(input != -9){
        mySet.insert(input);
        cin >> input;
    }
    int n;
    cin >> n;
    for(int i=0; i<n; i++){
        cin >> input;
        it = mySet.find(input);
        if(it != mySet.end()){
            cout << input << " ada" << endl;
        }else{
            cout << input << " tidak ada" << endl;
        }
    }
    return 0;
}
