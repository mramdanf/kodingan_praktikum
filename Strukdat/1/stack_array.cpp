#include<iostream>
#define SIZE 100

using namespace std;

class Stack {
    int st[SIZE];
    int atas;
public:
    Stack(){atas = SIZE;}
    bool empty(){return atas == SIZE;}
    bool full(){return atas == 0;}
    void push(int value);
    void pop();
    int top();
    int size(){return SIZE-atas;}
    //int getAtas() {return atas;}
    //int *getStack() {return stack;}
    void print();
};

void Stack::push(int val){
    if(!full()){
        --atas;
        st[atas]=val;
    }else
        cout << "FULL"<< endl;
}
void Stack::pop(){
    if(!empty()){
        ++atas;
    }
}
int Stack::top(){
    if(!empty()){
        return st[atas];
    }
}

void Stack::print(){
    if(empty()) cout << "kosong" << endl;
    else{
        for(int i=atas; i<SIZE; i++)
            cout << st[i] << endl;

    }
}



int main(){
    Stack st;
    st.push(50); st.push(15); st.push(20);
    cout << "Stack Awal\n";
    st.print();
    int nilai=st.top(); st.pop();
    cout << "\nHasil pop(): " << nilai << endl;
    cout << "\nStack Akhir\n";
    st.print();
    return 0;
}
