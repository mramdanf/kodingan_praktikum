#include<iostream>

#define M 100
#define N 100
using namespace std;


class MATRIK{
    int aray[M][N];
    int baris,kolom;

public :
    //MATRIK(){}
    void make(int aray[M][N], int baris, int kolom){
        this->baris = baris;
        this->kolom = kolom;
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                this->aray[i][j] = aray[i][j];
            }
        }
    }

    MATRIK transpose(){
        MATRIK t;
        int tArray[M][N];
        for(int i=0; i<this->kolom; i++){
            for(int j=0; j<this->baris; j++){
                tArray[i][j] = this->aray[j][i];
            }
        }
        t.make(tArray,this->kolom,this->baris);
        return t;
    }

    void print(){
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                cout << aray[i][j];
                if(j==kolom-1){
                    cout << endl;
                }else{
                    cout << " ";
                }
            }
        }
    }

    MATRIK operator+(MATRIK add){
        MATRIK t;
        int tambahArray[M][N];
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                tambahArray[i][j] = this->aray[i][j] + add.aray[i][j];
            }
        }
        t.make(tambahArray,this->baris,this->kolom);
    return t;
    }

    MATRIK operator*(MATRIK kali){
        MATRIK t;
        int kaliArray[M][N];
        for(int i=0; i<this->baris; i++){
            for(int j=0; j<kali.kolom; j++){
                kaliArray[i][j]=0;
                for(int h=0; h<this->kolom; h++)
                kaliArray[i][j] += this->aray[i][h]*kali.aray[h][j];
            }
        }
        t.make(kaliArray,this->baris,kali.kolom);
        return t;
    }
};
int main() {
   int arr[M][N]={{5,8,2},{8,3,1}};
   int brr[M][N]={{1,2,1},{0,1,1}};
   MATRIK A,B,C,D,BT;
   A.make(arr,2,3);    // buat matrik A ukuran 2x3
   B.make(brr,2,3);    // buat matrik B ukuran 2x3
   BT=B.transpose();
   C=A+B; C.print(); // menjumlah 2 matrik
   cout << endl;
   //BT.print();
   D=A*BT; D.print(); // mengalikan matrik
   return 0;
}
