#include<iostream>
#include<forward_list>
using namespace std;

template <typename T>
class SLL : public forward_list<T>{
	
	public:
		void print(){
			typename forward_list<T>::iterator it;
		for(it = forward_list<T>::begin(); it!= forward_list<T>::end(); ++it)
			cout << *it << "->";
		cout << "NULL" << endl;	
		}
		
};

int main(){
	
	SLL<float> dt;
	
	dt.push_front(20); dt.push_front(34);
	dt.print();
	return 0;
}
