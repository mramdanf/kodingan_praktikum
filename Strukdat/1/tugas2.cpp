#include<iostream>
//#include<string>

using namespace std;

//spesifikasi TYPE
struct node{
    string nim;
    float ipk;
    struct node *next;
};
typedef struct node Node;

class SLL2{
    Node* head;
    Node* tail;
public:
    void make(){ head=NULL; tail=NULL;}
    Node *make(string nim_, float ipk_);
    void push_back(string nim_, float ipk_);
    int isEmpty(){ return (head==NULL);}
    void push_front(string nim_, float ipk_);
    void push_after(string nim_, float ipk_,string nim1);
    Node *find(string nim1);
    void print();
    void del(string nim_,float ipk_);
};

Node* SLL2::make(string nim_,float ipk_){
    Node *nod = new(Node);
    nod->nim = nim_;
    nod->ipk = ipk_;
    nod->next = NULL;
    return nod;
}
void SLL2::push_back(string nim_,float ipk_){
    Node *p = make(nim_,ipk_);
    if(isEmpty()){
        head = p;
        tail = p;
    }else{
        tail->next = p;
        tail = p;
    }
}

void SLL2::push_front(string nim_, float ipk_){
    Node *p = make(nim_,ipk_);
    if(isEmpty()){ head = p; tail = p;}
    else{
        p->next = head;
        head = p;
    }
}

Node* SLL2::find(string nim1){
    Node *ptr = head;
    if(isEmpty()){return NULL;}
    else{
        while(ptr->next != NULL && ptr->nim != nim1){
            ptr = ptr->next;
        }
        if(ptr->nim == nim1){
            return ptr;
        }else return NULL;
    }
}

void SLL2::push_after(string nim_,float ipk_,string nim1){
    Node *ptr = find(nim1);
    if(ptr != NULL){
        Node *temp = make(nim_,ipk_);
        temp->next = ptr->next;
        ptr->next = temp;
    }
}

void SLL2::print(){
    Node *ptr = head;
    for(; ptr!=NULL; ptr=ptr->next){
        cout << "(" << ptr->nim << "," << ptr->ipk << ")" << "->";
    }
    cout << "NULL" << endl;
}

void SLL2::del(string nim_,float ipk_){

}

int main() {
   SLL2 list;
   list.make();
   list.push_back("G64204100", 3.14);
   list.push_back("G64204050", 3.67);
   list.push_front("G6420075", 2.05);
   list.print();
   list.push_after("G6420035", 2.89, "G64204100");
   list.print();// list.del("G64204100");
   //list.print();
   return 0;
}
