#include<iostream>
#include<forward_list>

using namespace std;

template <typename T>
class SLL : public forward_list<T>{
	public:
		void print(){
			typename forward_list<T>::iterator it;
		for(it = forward_list<T>::begin(); it!= forward_list<T>::end(); ++it){
			cout << *it << "->";
		} cout << "NULL" << endl;	
		}
		
		void push_back(T val){
			typename forward_list<T>::iterator it, before_end;
			if(forward_list<T>::empty()){
				forward_list<T>::insert_after(forward_list<T>::before_begin(),val);
			}else{
				for(it = forward_list<T>::begin(); it!= forward_list<T>::end(); ++it)
					before_end = it;
					forward_list<T>::insert_after(before_end,val);
				
			}
		}
		
};

int main(){
	
	SLL<int> dt;
	dt.push_front(30);
	dt.push_front(40);
	dt.push_back(20);
	dt.print();
	return 0;
}
