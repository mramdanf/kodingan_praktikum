#include<iostream>

using namespace std;

struct node{
    int info;
    struct node *prev, *next;
};
typedef struct node Node;

class DLL{
    Node *head, *tail;
 public:
    void make(){ head=NULL; tail=NULL;}
    Node *make(int val);
    int isEmpty(){ return (head==NULL);}
    void push_back(int val);
    void push_front(int val);
    void print();

};

Node* DLL::make(int val){
    Node *ptr = new (Node);
    ptr->info = val;
    ptr->next = NULL;
    ptr->prev = NULL;

    return ptr;
}

void DLL::push_back(int val){
    Node* ptr = make(val);
    if(isEmpty()){
        head = ptr;
        tail = ptr;
    }else{
        tail->next = ptr;
        ptr->prev = tail->next;
        tail = ptr;
    }
}
void DLL::push_front(int val){
    Node *ptr = make(val);
    if(isEmpty()){
        head = tail = ptr;
    }else{
        ptr->next = head;
        head->prev = ptr;
        head = ptr;
    }
}

void DLL::print(){
    Node *ptr = head;
    for(; ptr!=NULL;  ptr=ptr->next){
        cout << ptr->info << "->";
    }cout << "NULL"<< endl;
}

int main() {
    DLL list;
    list.make();
    list.push_back(100); list.push_back(50);list.print();
    list.push_front(75); list.print();
    //list.push_after(35,100); list.print();
    //list.del(50);

    return 0;
}
