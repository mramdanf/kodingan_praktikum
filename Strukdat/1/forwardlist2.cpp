#include <utility>
#include <iostream>
#include <forward_list>
#include <string>
using namespace std;
typedef pair<string, float> P;

class SLL {
SLLP dt; //
public:
	void push_front(string nim, float ipk);
	void push_back(string nim, float ipk);
	void push_after(string nim, float ipk,
	string after);
	void del(string nim);
	SLL::iterator find(string nim);//
	void print();
};

void SLL::print() {
	SLL::iterator it;
	for (it=dt.begin(); it!=dt.end(); ++it) {
	cout << "(" << it->first
	<< "," << it->second << ")->";
}
	cout << "NULL" << endl;
}

void SLL::push_front(string nim, float ipk) {
	P t=make_pair(nim, ipk);
	dt.push_front(t);
}


void SLL::push_back(string nim, float ipk) {
	P t=make_pair(nim, ipk);
	if (dt.empty()) dt.push_front(t);
	else {
	SLL::iterator before=dt.begin();
	SLL::iterator it=dt.begin();
	for (; it!=dt.end(); before=it, ++it);
	dt.insert_after(before,t);
	}
}

int main(){
	
	SLL at;
	at.push_front('G8374',8);
	at.print();
	
	return 0;
}
