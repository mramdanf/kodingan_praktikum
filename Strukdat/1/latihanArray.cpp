#include<iostream>
//#include<array>

using namespace std;

int main(){

    //array<int,3> myArray{10,20,30}; // sama dengan int myArray[3];
    //array<array<int,4>,2> x; // sama dengan int x[2][4];
    //array<array<array<int,5>,4>,3> x; // sama dengan int x[3][4][5]

    //for(int i=0; i<myArray.size(); i++){
    //    ++myArray[i];
    //    cout << myArray[i] << endl;
    //}

    int x[3] = {10,20,30};
    for(int i=0; i<3; ++i)
        //cout << x[i] << " ";
        ++x[i];

    for(int elem : x)
        cout << elem << '\n';

        return 0;
}
