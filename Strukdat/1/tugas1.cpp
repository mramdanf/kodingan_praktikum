#include<iostream>

#define M 100
#define N 100

using namespace std;

class MATRIK{

    int aray1[M][N];
    int baris, kolom;
public:


    void make(int aray11[M][N],int row,int col){
        baris = row;
        kolom = col;
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                aray1[i][j] = aray11[i][j];
            }
        }
    }

    void print(){
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                cout << aray1[i][j];
                if(j == kolom-1){
                    cout << endl;
                }else
                cout << " ";
            }
        }
    }

    MATRIK transpose(){
        MATRIK t;
        int array2[M][N];
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                array2[j][i] = aray1[i][j];
            }
        }
        t.make(array2,kolom,baris);
        return t;
    }

    MATRIK operator+(MATRIK add){
        MATRIK t;
        int jumlah[M][N];
        for(int i=0; i<baris; i++){
            for(int j=0; j<kolom; j++){
                jumlah[i][j] = aray1[i][j] + add.aray1[i][j];
            }
        }
        t.make(jumlah, baris, kolom);
        return t;
    }

    MATRIK operator*(MATRIK kali){
        MATRIK t;
        int ary1[M][N];
        for(int i=0; i<baris; i++){
            for(int j=0; j<kali.kolom; j++){
                   // ary1[i][j] = 0;
                for(int k=0; k<kolom; k++)
                    ary1[i][j] += aray1[i][k]*kali.aray1[k][j];
            }
        }
        t.make(ary1,baris,kali.kolom);
        return t;
    }
};

int main() {
   int arr[M][N]={{5,8,2},{8,3,1}};
   int brr[M][N]={{1,2,1},{0,1,1}};
   MATRIK A,B,C,D,BT;
   A.make(arr,2,3);    // buat matrik A ukuran 2x3
   B.make(brr,2,3);    // buat matrik B ukuran 2x3
   BT=B.transpose();
   //BT.print();
   C=A+B; C.print(); // menjumlah 2 matrik
   cout << endl;
   D=A*BT; D.print(); // mengalikan matrik
   return 0;
}
