#include <iostream>
#include <string>
#include <list>
using namespace std;

// {{

int main() {
    list<char> dt;
    list<char>::iterator it;
    string str;
    int k=0,max_l=0;

    cin >> str;
    for(int i=0;i<str.length();i++) {
        if(str[i]=='(' || str[i]=='{' || str[i]=='[' || str[i]=='<') {
            dt.push_front(str[i]);
            k++;
            if (k>max_l) max_l=k;
        } else {
            if (dt.front()=='(' && str[i]==')') {
                dt.pop_front();
                if(k<0) k--; else k=0;
            } else if (dt.front()=='{' && str[i]=='}') {
                dt.pop_front();
                if(k<0) k--; else k=0;
            } else if (dt.front()=='[' && str[i]==']') {
                dt.pop_front();
                if(k<0) k--; else k=0;
            } else if (dt.front()=='<' && str[i]=='>') {
                dt.pop_front();
                if(k<0) k--; else k=0;
            } else break;
        }
    }
    if (dt.empty() && str.length() > 1) cout << max_l << endl;
    else cout << "TIDAK VALID" << endl;
}
