#include <iostream>
#include <queue>
#include <string>
using namespace std;

class myQueue {
    int buffer;
    int maxBuffer;
    int notPrint, qSize;
    queue<string> dt;

    public:
        myQueue() {qSize=notPrint=buffer=0; maxBuffer=32;}

        void push_data(string f, int fSize) {
            if ((buffer+fSize) < maxBuffer) {
                dt.push(f);
                buffer+=fSize;
                ++qSize;
            } else ++notPrint;
        }

        void print() {
            while(!dt.empty()) {
                cout << dt.front() << endl;
                dt.pop();
            }
            cout << qSize << " " << notPrint << endl;
        }
};


int main() {
    int n, fileSize;
    string f;
    myQueue dt;

    cin >> n;
    while(n--) {
        cin >> f;
        cin >> fileSize;
        dt.push_data(f, fileSize);
    }
    dt.print();
    return 0;
}
