#include <iostream>
#define N 100 //kolom
#define M 100 //baris
using namespace std;

class MATRIK {
	int dt[M][N];
	int nRow, nCol;
	int i,j;
	
	public:
		void make(int arr[M][N], int row, int col) {
			nRow=row; nCol=col;
			for(i=0; i<row; i++) {
				for(j=0; j<col; j++) {
					dt[i][j] = arr[i][j];
				}
			}
		}
		
		void print() {
			for(i=0; i<nRow; i++) {
				for(j=0; j<nCol; j++) {
					cout << dt[i][j];
					if (j==nCol-1) cout << endl;
					else cout << " ";
				}
			}
		}
		
		MATRIK transpose() {
			MATRIK mt;
			int tmp[M][N];
			for(i=0; i<nCol; i++) {
				for(j=0; j<nRow; j++) {
					tmp[i][j] = dt[j][i];
				}
			}
			mt.make(tmp, nCol, nRow);
			return mt;
		}
		
		int *getMatrik() {
			return *dt;
		}
		
		int getRow() {
			return nRow;
		}
		
		int getCol() {
			return nCol;
		}
		
		friend MATRIK operator+(MATRIK a, MATRIK b) {
			int hasil[M][N];
			MATRIK result;
			for (int i=0; i<a.getRow(); i++) {
				for(int j=0; j<a.getCol(); j++) {
					hasil[i][j] = a.dt[i][j] + b.dt[i][j];
				}
			}
			
			result.make(hasil, a.getRow(), a.getCol());
			return result;
		}
		
		friend MATRIK operator*(MATRIK a, MATRIK b) {
			 MATRIK t;
	        int kaliArray[M][N];
	        for(int i=0; i<a.getRow(); i++){
	            for(int j=0; j<b.getCol(); j++){
	                kaliArray[i][j]=0;
	                for(int h=0; h<a.nCol; h++)
	                kaliArray[i][j] += a.dt[i][h]*b.dt[h][j];
	            }
	        }
	        t.make(kaliArray,a.nRow,b.nCol);
	        return t;
		}
};

int main() {
	int arr[M][N]={{5,8,2},{8,3,1}};
   int brr[M][N]={{1,2,1},{0,1,1}};
   MATRIK A,B,C,D,BT;
   A.make(arr,2,3);    // buat matrik A ukuran 2x3
   B.make(brr,2,3);    // buat matrik B ukuran 2x3
   BT=B.transpose();
   C=A+B; C.print(); // menjumlah 2 matrik
   cout << endl;
   D=A*BT; D.print(); // mengalikan matrik
   return 0;
}
