#include <iostream>
#include <list>
using namespace std;

template<typename T>
class ODLL : public list<T> {
   public:
      void push(T val) {
        this->push_back(val);
        this->sort();
      }
};

int main() {
    ODLL<int> list;
   int n, val;
   cin >> n;
   while (n--) {
	   cin >> val;
	   list.push(val);
   }
   ODLL<int>::iterator it;
   for (it=list.begin(); it!=list.end(); ++it) cout << *it << "<->";
   cout << "NULL\n";
   return 0;
}
