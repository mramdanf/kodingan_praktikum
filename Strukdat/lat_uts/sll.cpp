#include <iostream>
using namespace std;

struct node {
	int info;
	struct node *next;
};
typedef struct node Node;

class SLL {
	Node *head, *tail;
	public:
		void make() {head=NULL; tail=NULL;}
		Node *make(int val);
		int isEmpty() { return (head==NULL); }
		void print();
		void push_back(int val);
		void push_front(int val);
		Node *find_before(int val);
		void del(int val);
		void push_after(int val, int before);
		Node *find(int val);
};
Node *SLL::make(int val) {
	Node *ptr = new (Node);
	ptr->info = val;
	ptr->next = NULL;
	return ptr;
}
void SLL::push_back(int val) {
	Node *ptr = make(val);
	if (isEmpty()) head=tail=ptr;
	else {
		tail->next=ptr;
		tail=ptr;
	}
}
void SLL::push_front(int val) {
	Node *ptr = make(val);
	if (isEmpty()) head=tail=ptr;
	else {
		ptr->next=head;
		head=ptr;
	}
}
void SLL::push_after(int val, int before) {
	Node *ptr = find(before);
	if(ptr!=NULL) {
		Node *baru = make(val);
		baru->next = ptr->next;
		ptr->next = baru;
	}	
}
Node *SLL::find_before(int val) {
	Node *ptr = head;
	Node *pra = head;
	while(ptr!=NULL && ptr->info!=val) {
		pra = ptr;
		ptr=ptr->next;
	}		
	if (ptr->info==val) return pra;
	else return NULL;
}
Node *SLL::find(int val) {
	Node *ptr = head;
	while(ptr!=NULL && ptr->info!=val) {
		ptr=ptr->next;
	}
	if (ptr->info==val) return ptr;
	else return NULL;
}
void SLL::del(int val) {
	Node *ptr = find(val);
	if (ptr==head) head=head->next;
	else {
		ptr = find_before(val);
		if (ptr!=NULL) {
			if (ptr->next==tail) tail=ptr;
			ptr->next=(ptr->next)->next;
		}
	}	
}
void SLL::print() {
	Node *ptr = head;
	while(ptr!=NULL) {
		cout << ptr->info << "->";
		ptr=ptr->next;
	}
	cout << "NULL" << endl;
}

int main() {
	SLL list;
	list.make();
	
	list.push_back(100); list.push_back(50);
	list.push_front(75); list.print();
	
	list.push_after(35,100); list.print();
	
	list.del(35); list.print();
}
