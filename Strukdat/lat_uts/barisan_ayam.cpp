#include <iostream>
#include <list>
using namespace std;

typedef list<int> li;
class DLL {
	li dt;
	public:
		int isEmpty() {return dt.empty();}
		void push_back(int val) {dt.push_back(val);}
		void push_front(int val) {dt.push_front(val);}
		void print();
		void push_after(int val, int before);
		void del(int val);
		void push_ayam(int val);
		li::iterator find(int val);
};
void DLL::push_ayam(int val) {
	li::iterator it = dt.begin();
	while(it!=dt.end() && *it < val) ++it;
	if (it!=dt.end()) dt.insert(it,val);
	else dt.push_back(val);
}
void DLL::push_after(int val, int before) {
	if (!isEmpty()) {
		li::iterator it = find(before);
		if (it!=dt.end()) {
			++it;
			dt.insert(it,val);
		}
	}
}
li::iterator DLL::find(int val) {
	li::iterator it=dt.begin();
	while(it!=dt.end() && *it!=val) ++it;
	return it;
}
void DLL::del(int val) {
	li::iterator it=find(val);
	if (it!=dt.end()) dt.erase(it);
}
void DLL::print() {
	li::iterator it;
	for(it=dt.begin();it!=dt.end();++it) {
		cout << *it << "->";
	}
	cout << "NULL" << endl;
}
int main() {
	DLL list;
	int m,n,t;
	
	cin >> n;
	while(n--) {
		cin >> t;
		list.push_back(t);
	}
	cin >> m;
	while(m--) {
		cin >> t;
		list.push_ayam(t);
	}
	list.print();
	return 0;
}
