#include <iostream>
#include <string>
#include <stack>
using namespace std;

typedef stack<string> ss;

class myStack {
	ss dt;
	
	public:
		void push(string s) {
			dt.push(s);
		}
		
		void print() {
			while(!dt.empty()) {
				cout << dt.top() << " ";
				dt.pop();
			}
			cout << endl;
		}
};

int main() {
	myStack st;
	string s;
	
	cin >> s;
	while(s.compare("STOP") != 0) {
		st.push(s);
		cin >> s;
	}
	st.print();
	return 0;
}
