#include <iostream>
#include <queue>
#define ECONOMY_CLASS 0
#define BUSINESS_CLASS 1
#define EXECUTIVE_CLASS 2
#define MAX_PASSENGER 1000
using namespace std;

struct Passenger
{
    int id;
    int serviceTime;
    int priority;
};

struct Node
{
    Passenger passenger;
    Node *prev, *next;
};

bool operator<(const Passenger a, const Passenger b)
{
    return a.priority < b.priority;
}

class Queue
{
    Node *Front, *Rear;
	public:
	    Queue() {
	        Front = NULL;
	        Rear = NULL;
	    };
	    bool empty(){return (Front == NULL);};
	    void push(Passenger p);
	    Passenger front();
	    Passenger back();
	    void pop();
	    void print();
	    void count();
	    void urut_id();
};

int main()
{
    int n,sTime;
    char prior;
    Queue myq;
    
    cin >> n;
    for(int i=1;i<=n;i++) {
    	cin >> prior >> sTime;
    	Passenger pass;
    	if (prior=='X') pass.priority = EXECUTIVE_CLASS;
    	else if (prior=='B') pass.priority = BUSINESS_CLASS;
    	else if (prior=='E') pass.priority = ECONOMY_CLASS;		
		pass.id = i;
    	pass.serviceTime = sTime;
    	myq.push(pass);
	}
	myq.count();
	myq.print();
	myq.urut_id(n);
	
}
void Queue::push(Passenger p)
{
    Node *temp = new(Node);
    temp->passenger = p;
    temp->prev=NULL;
    temp->next=NULL;
    if(empty())
    {
        // TO DO
        Front=Rear=temp;
    }
    else
    {
        // TO DO
        Node *ptr = Front;
        Node *pra = Front;
        if (p.priority == Front->passenger.priority) {
        	while(ptr!=NULL && p.priority == ptr->passenger.priority) {
        		pra=ptr;
        		ptr=ptr->next;
			}
			if (ptr==NULL) { // push back
				Rear->next = temp;
				temp->prev = Rear;
				Rear = temp;
			} else { // push after
				temp->prev = pra;
				temp->next = pra->next;
				(pra->next)->prev = temp;
				pra->next = temp;
			}
		
		} else if (p.priority > Front->passenger.priority) {
			temp->next = Front;
			Front->prev = temp;
			Front = temp;
		} else if (p.priority < Front->passenger.priority) {
			while(ptr!=NULL && p.priority <= ptr->passenger.priority) {
				pra=ptr;
				ptr=ptr->next;
			}
			if (ptr==NULL) { // push back
				Rear->next = temp;
				temp->prev = Rear;
				Rear = temp;
			} else { // push after
				temp->prev = pra;
				temp->next = pra->next;
				(pra->next)->prev = temp;
				pra->next = temp;
			}
		}
		
        
    }
}
void Queue::pop()
{
    if(empty())
        cout << "Queue is empty\n";
    else
        Front = Front -> next;
}
Passenger Queue::front()
{
    if(!empty())
        return Front -> passenger;
}

Passenger Queue::back()
{
    if(!empty())
        return Rear-> passenger;
}

void Queue::print() {
	Node *ptr = Front;
	while(ptr!=NULL) {
		//cout << ptr->passenger.priority << " " << ptr->passenger.serviceTime << " " << ptr->passenger.id << endl;
		cout << ptr->passenger.id << endl;
		ptr=ptr->next;
	}
}

void Queue::count() {
	Node *ptr = Rear;
	int a;
	while(ptr!=NULL) {
		a=0;
		Node *c = ptr->prev;
		while(c!=NULL) {
			a+=c->passenger.serviceTime;
			c=c->prev;
		}
		ptr->passenger.serviceTime=a;
		ptr=ptr->prev;
	}
}

void Queue::urut_id() {
	queue<Node> qu;
	int min;
	Node n;	
	
	Node *ptr=Front;
	while(ptr!=NULL) {
		Node *pra = ptr;
		Node *min = ptr;
		while(pra != NULL) {
			if (pra->passenger.id < min->passenger.id)
				min=pra;
			pra=pra->next;
		}
		qu.push(*min);
		ptr=ptr->next;
	}
	
	// print
	while(!qu.empty()) {
		n = qu.front();
		cout << n.passenger.id << " ";
		qu.pop();
	}
	
	cout << endl;
}
