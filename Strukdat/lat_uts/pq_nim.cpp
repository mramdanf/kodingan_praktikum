#include <iostream>
#include <string>
#include <queue>
using namespace std;
struct record {
string nim;
int gender;
double tinggi;
};
typedef struct record Record;
typedef priority_queue<Record> PQ;
bool operator<(const Record a, const Record b)
{
return a.tinggi < b.tinggi;
}
ostream& operator<< (ostream &out, Queue q) {
if (q.empty()) out << "Stack is empty\n";
else {
while (!q.empty()) {
out << q.front() << endl;
q.pop();
}
}
return out;
}

int main() {
PQ antrian;
int n; cin >> n;
Record t;
while (n--) {
cin >> t.nim >> t.gender >> t.tinggi;
antrian.push(t);
}
cout << "PQ awal:\n";
cout << antrian;
return 0;
}

