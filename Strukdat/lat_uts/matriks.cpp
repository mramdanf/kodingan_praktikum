#include <iostream>
using namespace std;
#define M 100
#define N 100

class MATRIK {
	int dt[M][N];
	int nRow, nCol,i,j;
	
	public:
		void make(int arr[M][N], int row, int col) {
			nRow=row; nCol=col;
			for(i=0;i<row;i++) {
				for(j=0;j<col;j++) {
					dt[i][j]=arr[i][j];
				}
			}
		}
		
		MATRIK transpose() {
			MATRIK mt;
			int tmp[M][N];
			for(i=0;i<nCol;i++) {
				for(j=0;j<nRow;j++) {
					tmp[i][j] = dt[j][i];
				}
			}
			mt.make(tmp, nCol, nRow);
			return mt;
		}
		
		void print() {
			for(i=0;i<nRow;i++) {
				for(j=0;j<nCol;j++) {
					cout << dt[i][j];
					if (j!=nCol-1) cout << " ";
					else cout << endl;
				}
			}
		}
		
		friend MATRIK operator+(MATRIK a, MATRIK b) {
			int hasil[M][N];
			MATRIK mt;
			for(int i=0;i<a.getRow();i++) {
				for(int j=0;j<b.getCol();j++) {
					hasil[i][j]=a.dt[i][j]+b.dt[i][j];
				}
			}
			mt.make(hasil, a.getRow(), a.getCol());
			return mt;
		}
		
		friend MATRIK operator*(MATRIK a, MATRIK b) {
			int hasil[M][N];
			MATRIK mt;
			for(int i=0;i<a.getRow();i++) {
				for(int j=0;j<b.getCol();j++) {
					hasil[i][j]=0;
					for(int k=0;k<a.getCol();k++)
						hasil[i][j]+=a.dt[i][k]*b.dt[k][j];
				}
			}
			mt.make(hasil, a.getRow(), b.getCol());
			return mt;
		}
		
		int getCol() {
			return nCol;
		}
		
		int getRow() {
			return nRow;
		}
};

int main() {
	int arr[M][N]={{5,8,2},{8,3,1}};
	int brr[M][N]={{1,2,1},{0,1,1}};
	MATRIK A,B,C,D,BT;
	A.make(arr,2,3); B.make(brr,2,3);
	BT=B.transpose();
	C=A+B; C.print(); // menjumlah 2 matrik
	D=A*BT; D.print(); // mengalikan matrik
	return 0;
}
