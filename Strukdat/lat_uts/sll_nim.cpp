#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

struct node {
	string nim;
	float nilai;
	struct node *next;
};
typedef struct node Node;

class SLL {
	Node *head, *tail;
	public:
		void make() {head=NULL; tail=NULL;}
		Node *make(string nim, float nilai);
		int isEmpty() { return (head==NULL); }
		void print();
		void push_back(string nim, float nilai);
		void push_front(string nim, float nilai);
		Node *find_before(string nim);
		void del(string nim);
		void push_after(string nim, float nilai, string before);
		Node *find(string nim);
};
Node *SLL::make(string nim, float nilai) {
	Node *ptr = new (Node);
	ptr->nim = nim;
	ptr->nilai = nilai;
	ptr->next = NULL;
	return ptr;
}
void SLL::push_back(string nim, float nilai) {
	Node *ptr = make(nim, nilai);
	if (isEmpty()) head=tail=ptr;
	else {
		tail->next=ptr;
		tail=ptr;
	}
}
void SLL::push_front(string nim, float nilai) {
	Node *ptr = make(nim, nilai);
	if (isEmpty()) head=tail=ptr;
	else {
		ptr->next=head;
		head=ptr;
	}
}
void SLL::push_after(string nim, float nilai, string before) {
	Node *ptr = find(before);
	if(ptr!=NULL) {
		Node *baru = make(nim, nilai);
		baru->next = ptr->next;
		ptr->next = baru;
	}	
}
Node *SLL::find(string nim) {
	Node *ptr = head;
	while(ptr!=NULL && ptr->nim!=nim) {
		ptr=ptr->next;
	}
	if (ptr->nim==nim) return ptr;
	else return NULL;
}
Node *SLL::find_before(string nim) {
	Node *ptr = head;
	Node *pra = head;
	while(ptr!=NULL && ptr->nim!=nim) {
		pra = ptr;
		ptr=ptr->next;
	}		
	if (ptr->nim==nim) return pra;
	else return NULL;
}

void SLL::del(string nim) {
	Node *ptr = find(nim);
	if (ptr==head) head=head->next;
	else {
		ptr = find_before(nim);
		if (ptr!=NULL) {
			if (ptr->next==tail) tail=ptr;
			ptr->next=(ptr->next)->next;
		}
	}	
}
void SLL::print() {
	Node *ptr = head;
	while(ptr!=NULL) {
		cout << setprecision(3);
		cout << "(" << ptr->nim << "," << ptr->nilai << ")->";
		ptr=ptr->next;
	}
	cout << "NULL" << endl;
}

int main() {
	SLL list;
	list.make();
	list.push_back("G64204100", 3.14);
	list.push_back("G64204050", 3.67);
	list.push_front("G6420075", 2.05);
	list.print();
	list.push_after("G6420035", 2.89, "G64204100");
	list.print(); list.del("G64204100");
	list.print();

}
