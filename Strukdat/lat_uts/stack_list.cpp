#include <iostream>
#include <list>
using namespace std;

typedef list<int> li;
class Stack {
	li dt;
	li::iterator atas;
	public:
		Stack() {atas=dt.end();}
		void push(int val);
		int empty() {return dt.empty();}
		void print();
		int size() {return dt.size();}
		int top() {return *atas;}
		void pop();
		li::iterator getAtas() {return atas;}
};
void Stack::push(int val) {
	dt.push_front(val);
	atas = dt.begin();
}
void Stack::print() {
	li::iterator it = getAtas();
	for(int i=0; i < size(); i++) {
		cout << *it << " ";
		it++;
	}
	cout << endl;
}
void Stack::pop() {
	if (empty()) cout << "Stack is empty\n";
	else {
		++atas;
		dt.pop_front();
	}
}
int main() {
	Stack st;
	st.push(50); st.push(15); st.push(20);
	cout << "Stack Awal\n";
	st.print();
	int nilai=st.top(); st.pop();
	cout << "\nHasil pop(): " << nilai << endl;
	cout << "\nStack Akhir\n";
	st.print();		
	return 0;
}
