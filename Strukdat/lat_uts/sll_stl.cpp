#include <iostream>
#include <forward_list>
using namespace std;

template<typename T>

class SLL : public  forward_list<T> {
    typename forward_list<T>::iterator it;

    public:
        void print() {
            for(it=forward_list<T>::begin(); it!=forward_list<T>::end(); ++it) {
                cout << *it << "->";
            }
            cout << "NULL" << endl;
        }

        void push_back(T val) {
            typename forward_list<T>::iterator before;
            if (forward_list<T>::empty()) forward_list<T>::push_front(val);
            else {
                it=forward_list<T>::begin();
                while(it!=forward_list<T>::end()) {
                    before=it;
                    ++it;
                }
                forward_list<T>::insert_after(before, val);
            }
        }

		void push_after(T val, T after) {
			if (!forward_list<T>::empty()) {
				it = find(after);
				if (it!=forward_list<T>::end()) {
					forward_list<T>::insert_after(it, val);
				}
			}
		}
		
		void del(T val) {
			if (!forward_list<T>::empty()) {
				it = find_before(val);
				if (it!=forward_list<T>::end()) {
					forward_list<T>::erase_after(it);
				}
			}
		}
		
		typename forward_list<T>::iterator find(T val) {
			 it = forward_list<T>::begin();
			
			while(it!=forward_list<T>::end() && *it!=val) it++;			
			return it;
		}
		
		typename forward_list<T>::iterator find_before(T val) {
			it = forward_list<T>::begin();
			
			typename forward_list<T>::iterator before;
			while(it!=forward_list<T>::end() && *it!=val){
				before = it;
				it++;	
			} 			
			return before;
		}
};

int main() {
    SLL<int> dt;
    dt.push_back(100); dt.push_back(50);
	dt.push_front(75); dt.print();
	
	dt.push_after(35,100); dt.print();
	
	dt.del(35); dt.print();
	return 0;
}
