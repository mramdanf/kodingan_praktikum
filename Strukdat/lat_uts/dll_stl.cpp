#include <iostream>
#include <list>
using namespace std;

typedef list<int> li;
class DLL {
	li dt;
	public:
		int isEmpty() {return dt.empty();}
		void push_back(int val) {dt.push_back(val);}
		void push_front(int val) {dt.push_front(val);}
		void print();
		void push_after(int val, int before);
		void del(int val);
		li::iterator find(int val);
};
void DLL::push_after(int val, int before) {
	if (!isEmpty()) {
		li::iterator it = find(before);
		if (it!=dt.end()) {
			++it;
			dt.insert(it,val);
		}
	}
}
li::iterator DLL::find(int val) {
	li::iterator it=dt.begin();
	while(it!=dt.end() && *it!=val) ++it;
	return it;
}
void DLL::del(int val) {
	li::iterator it=find(val);
	if (it!=dt.end()) dt.erase(it);
}
void DLL::print() {
	li::iterator it;
	for(it=dt.begin();it!=dt.end();++it) {
		cout << *it << "->";
	}
	cout << "NULL" << endl;
}
int main() {
	DLL list;
	list.push_back(100); list.push_back(50);
	list.push_front(75); list.print();
	list.push_after(35,100); list.print();
	list.del(100); list.print();
	return 0;
}
