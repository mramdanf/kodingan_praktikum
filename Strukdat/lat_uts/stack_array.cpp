#include <iostream>
#define SIZE 100
using namespace std;

class Stack {
	int stack[SIZE];
	int atas;
	
	public:
		Stack() {atas=SIZE;}
		void push(int val);
		int full() {return atas==0;}
		int empty() {return atas==SIZE;}
		int getAtas() {return atas;}
		int *getStack() {return stack;}
		int size() {return SIZE-atas;}
		int top();
		void pop();
		void print();

		
};
void Stack::print() {
	if (empty()) cout << "Stack is empty\n";
	else {
		for(int i=getAtas(); i<getAtas()+size(); i++) {
			cout << getStack()[i] << " ";
		}
		cout << endl;
	}
}
int Stack::top() {
	if (empty()) {
		cout << "Stack is empty\n"; return -1;
	} else return stack[atas];
}
void Stack::push(int val) {
	if (full()) cout << "Stack is full\n";
	else stack[--atas]=val;
}
void Stack::pop() {
	if (empty()) cout << "Stack is empty\n";
	else ++atas;
}

int main() {
	Stack st;
	st.push(50); st.push(15); st.push(20);
	cout << "Stack Awal\n";
	st.print();
	int nilai=st.top(); st.pop();
	cout << "\nHasil pop(): " << nilai << endl;
	cout << "\nStack Akhir\n";
	st.print();		
	return 0;
}
