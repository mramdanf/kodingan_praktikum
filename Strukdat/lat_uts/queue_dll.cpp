#include <iostream>
using namespace std;

typedef struct node {
	int value;
	struct node *prev, *next;
} Node;

class Queue {
	Node *Front, *Rear;
	
	public:
		Queue() {Front=Rear=NULL;}
		bool empty() {return Front==NULL;}
		void push(int val);
		int front();
		int back();
		void pop();
		void print();
		Node *make(int val);
};

Node *Queue::make(int val) {
	Node *ptr = new(Node);
	ptr->value = val;
	ptr->next = ptr->prev = NULL;
	return ptr;
}

void Queue::push(int val) {
	Node *ptr = make(val);
	if (empty()) Front=Rear=ptr;
	else {
		Rear->next = ptr;
		ptr->prev = Rear;
		Rear = ptr;
	}
}

int Queue::front() {
	if(empty()) {cout << "Stack is empty" << endl; return -1;}
	else {
		return Front->value;
	}
}

int Queue::back() {
	if(empty()) {cout << "Stack is empty" << endl; return -1;}
	else {
		return Rear->value;
	}
}

void Queue::pop() {
	if(empty()) cout << "Stack is empty" << endl;
	else {
		Front = Front->next;
	}	
}

void Queue::print() {
	Node *ptr = Front;
	while(ptr!=NULL) {
		cout << ptr->value << "<-";
		ptr=ptr->next;
	}
	cout << "NULL" << endl;
}

int main() {
	Queue antrian;
	antrian.push(10); antrian.push(20);
	antrian.push(15); antrian.push(7);
	cout << "Queue awal:\n";
	antrian.print();
	cout << "\nFront: " << antrian.front() << endl;
	cout << "\nRear: " << antrian.back() << endl;
	antrian.pop(); antrian.pop();
	cout << "\nQueue akhir:\n";
	antrian.print();

	return 0;
}
