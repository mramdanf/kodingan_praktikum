#include <iostream>
#include <string>
using namespace std;


struct node {
	int info;
	struct node *next, *prev;
};
typedef struct node Node;

class DLL {

	protected:
        Node *head, *tail;

	public:
		void make() {head=tail=NULL;}
		Node *make(int val);
		void push_back(int val);
		void print();
		void push_front(int val);
		void push_after(int val, int before);
		Node *find(int val);
		void del(int val);
		int isEmpty() {return head==NULL;}
};

Node *DLL::make(int val) {
	Node *ptr = new(Node);
	ptr->info = val;
	ptr->next = ptr->prev = NULL;
	return ptr;
}

void DLL::push_back(int val) {
	Node *ptr = make(val);
	if (isEmpty()) head=tail=ptr;
	else {
		tail->next=ptr;
		ptr->prev=tail;
		tail=ptr;
	}
}

Node *DLL::find(int val) {
	Node *ptr = head;
	while(ptr!=NULL && ptr->info!=val) ptr=ptr->next;

	if (ptr->info==val) return ptr;
	else return NULL;
}

void DLL::push_after(int val, int before) {
	Node *ptr = find(before);
	if (ptr!=NULL) {
		Node *temp = make(val);
		temp->prev = ptr;
		temp->next = ptr->next;
		(ptr->next)->prev = temp;
		ptr->next = temp;
	}
}
void DLL::print() {
	Node *ptr = head;
	while(ptr!=NULL) {
		cout << ptr->info << "->";
		ptr=ptr->next;
	}
	cout << "NULL" << endl;
}

void DLL::push_front(int val) {
	Node *ptr = make(val);
	if (isEmpty()) head=tail=ptr;
	else {
		head->prev = ptr;
		ptr->next = head;
		head=ptr;
	}
}

void DLL::del(int val) {
	Node *ptr = find(val);
	if (ptr!=NULL) {
		if (ptr==head) head=head->next;
		else if (ptr==tail) {
			tail=tail->prev;
			tail->next = NULL;
		}
		else if (ptr!=NULL) {
			(ptr->prev)->next = ptr->next;
			(ptr->next)->prev = ptr->prev;
		}
	}
}

class myStack : public DLL {

    public:

        void push(int val) {
            push_front(val);
        }

        void print_s() {
            Node *ptr = head;
            cout << "stack: ";
            if (ptr!=NULL) {
                while(ptr!=NULL) {
                    cout << ptr->info << "->";
                    ptr=ptr->next;
                }
            } else {
                cout << "empty" << endl;
            }

        }
};

int main() {
    string a;
    int v;
    myStack s;

    cin >> a;
    while(a.compare("q") != 0) {
        if (a.compare("push") == 0) {
            cin >> v;
            s.push(v);
            s.print();
        }
        cin >> a;
    }
	return 0;
}
