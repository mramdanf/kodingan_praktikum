#include <iostream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

struct node {
    string path;
    struct node* next;
};
typedef struct node Node;

class SLL {
Node *head, *tail;

public:
    void make() { head=NULL; tail=NULL;}
    Node *make(string path);
    int isEmpty() { return (head==NULL); }
    void push_back(string path);
    void push_front(string nim, float nilai);
    void push_after(string nim, float nilai, string after);
    Node *find(string nim);
    Node *find_before(string nim);
    void del(string nim);
    void print();
};
Node *SLL::make(string path) {
	Node *ptr = new(Node);
	ptr->path = path;
	ptr->next = NULL;
	return ptr;
}
void SLL::push_back(string path) {
	Node *ptr = make(path);
	if (isEmpty()) {head=ptr; tail=ptr;}
	else {
		tail->next=ptr; tail=ptr;
	}
}
void SLL::print() {
    Node *ptr=head;
    for (; ptr!=NULL; ptr=ptr->next)
        cout << ptr->path << "/";
    cout << endl;
}
int main() {
   SLL list;
   char tmp[256];
   char *pch;
   string path;
   double nilai;
   int n,i;
   
   list.make();

	scanf("%d ", &n);
	
	for(i=0;i<n;i++) {
		cin.getline(tmp, 256);
		pch = strtok (tmp, " ");
		
		if (strcmp("next", pch) == 0) {
			pch = strtok(NULL, " "); 
			path = pch;
			list.push_back(path);
		}
	}
	list.print();
	
	
	
   
   

   return 0;
}
