#include <iostream>
#include <list>
using namespace std;

template<typename T>
class ODLL : public list<T> {
    typename list<T>::iterator ptr;
    typename list<T>::iterator before;
   public:
      void push(T val) {
        for(ptr=this->begin(); ptr!=this->end(); ++ptr) {
            if (val < *ptr) break;
            else {
                before=ptr;
            }
        }
        if (ptr==this->end()) {
            this->push_back(val);
        } else if (ptr == this->begin()) {
            this->push_front(val);
        } else {
            this->insert(ptr, val);
        }
      }
};

int main() {
    ODLL<int> list;
   int n, val;
   cin >> n;
   while (n--) {
	   cin >> val;
	   list.push(val);
   }
   ODLL<int>::iterator it;
   for (it=list.begin(); it!=list.end(); ++it) cout << *it << "<->";
   cout << "NULL\n";
   return 0;
}
