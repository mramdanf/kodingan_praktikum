#include<iostream>
#include<set>


using namespace std;

int main(){

    set<int> data;
    int nilai;
    cin >> nilai;   //nilai yang sama jika menggunakan set maka hanya akan di print satu saja
    set<int>::iterator it;

    while(nilai != -9){
        data.insert(nilai);
        cin >> nilai;
    }
    int n;
    cin >> n;
    for(int i=0; i<n; i++){
        cin >> nilai;
        it = data.find(nilai); //mencari nilai yang sama dengan data yang di simpan dalam set. jika sama maka akan disimpan dalam it
        if(it != data.end()) cout << *it << " ada\n";   //jika nilai it tidak sama dengan data.end() (maksudnya data.end() adalah data yang tidak ada dalam set atau setelah data terakhir dalam set.) maka akan
        else{
            cout << nilai << " tidak ada\n";
        }
    }

    return 0;
}
