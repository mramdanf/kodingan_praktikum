#include<iostream>
#include<string>
using namespace std;

void convert(int n)
{
    if (n/2!=0) {
        convert(n/2);
    }
    cout << n%2;
}

int main(){
    int n;
    cin >> n;
    for(int i=1;i<=n;i++){
        convert(i);
        cout << endl;
    }
    return 0;
}
