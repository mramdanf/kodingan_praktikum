// stack::top
#include <iostream>       // std::cout
#include <stack>          // std::stack
using namespace std;
int main ()
{
  stack<int> mystack;
	for(int i=0;i<6;i++) mystack.push(i);
	while(!mystack.empty())
	{
		cout << mystack.top()<<endl;
		mystack.pop();
	}

  return 0;
}
