#include <iostream>
#include <list>
#include <iomanip>

using namespace std;

class Person {
private:
     string name;
     int age;
     int height;
     double weight;
     double imt;
     string status;
     double IMT() { double h=(double)height/100.0; return (weight/(h*h)); }
     string LastStatus() {
        double h=IMT();
        if (h<17.0) return "sangat kurus";
        else if (h<18.5) return "kurus";
        else if (h<25.0) return "normal";
        else if (h<28.0) return "gemuk";
        else return "sangat gemuk"; }
public:
     Person() { name=""; age=height=0; weight=0.0; }
     void setPerson(string n, int a, int h, double w) {
        name=n; age=a; height=h; weight=w;
        imt=IMT(); status=LastStatus();}
     string getName() { return name; }
     int getAge() { return age; }
     int getHeight() { return height; }
     double getWeight() { return weight; }
     double getIMT() { return imt; }
     string getLastStatus() { return status; }
     void print() {
         cout << name << " " << age << " " << height << " ";
         cout << fixed << setprecision(2) << weight << " " << imt << " " << status << endl;
      }
};

int main()
{
    Person p;
    int n,insert,input,age,height;
    float weight;
    string name;
        list<Person> P;
        list<Person>::iterator ptr;
   cin >> n;
   for(int i=0;i<n;i++){
        cin >> name >> age >> height >> weight;
        p.setPerson(name,age,height,weight);
        P.push_back(p);}
   while(cin >> input, input != -9){
        if(input == 1){
            cin >> name >> age >> height >> weight;
            p.setPerson(name,age,height,weight);
            P.push_front(p);
        }else if(input == 2){
            cin >> name >> age >> height >> weight;
            p.setPerson(name,age,height,weight);
            P.push_back(p);
        }else if(input == 3){
            P.pop_front();
        }else if(input == 4){
            P.pop_back();
        }
        else if(input == 5){
            cin >> insert;
            cin >> name >> age >> height >> weight;
            p.setPerson(name,age,height,weight);
            ptr = P.begin();
            for(int i=0; i < insert; i++)
                ++ptr;
            P.insert(ptr,p);
        }
    }

   list<Person>::iterator it;

   for(it=P.begin(); it != P.end(); ++it)
        it->print();
   return 0;
}
