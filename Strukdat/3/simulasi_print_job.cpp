#include<iostream>
#include<queue>
#include<string>
using namespace std;

int main(){
    queue<string> p;
    string namaFile;
    int n, jumlahMB = 31, mb, jPrint=0, jtPrint=0;

    cin >> n;

        while(n--){
            cin >> namaFile >> mb;
            jumlahMB = jumlahMB-mb;

            if(jumlahMB >= 0){
                p.push(namaFile);
                jPrint++;
                cout << namaFile << endl;
            }else{
                jumlahMB = jumlahMB+mb;
                jtPrint++;
            }
        }
        cout << jPrint << " " << jtPrint << endl;
    return 0;
}
