#include <iostream>
#include <stack>
#include <string>

using namespace std;

int cekSama(char opening, char closing){
    if (opening == '[' && closing == ']') return true;
    else if (opening == '{' && closing == '}') return true;
    else if (opening == '(' && closing == ')') return true;
    else if (opening == '<' && closing == '>') return true;
    else return false;

}
int main(){
    stack<char> mystack;

    int count=0, max=0;
    string input;
    cin>> input;

    for (int i=0;i<input.length();i++){

            if (input[i]=='('||input[i]== '[' || input[i] == '{' || input[i]=='<'){
                    mystack.push(input[i]);
                    count++;
                    if(max<count)max = count;
                }else{
                    if(mystack.empty()){
                        cout<<"TIDAK VALID\n";
                        return 0;
                    }
                    if(cekSama(mystack.top(),input[i])){
                        mystack.pop();
                        count=0;

                    }
                    else {
                        cout<<"TIDAK VALID\n";
                        return 0;
                    }
                }

    }
    cout<<max<<endl;

    return 0;

}
