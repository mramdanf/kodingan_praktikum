#include<iostream>
#include<stack>
#include<set>


using namespace std;
typedef stack<char> Stack;

int main(){
    //stack<char> myStack;
    Stack myStack;
    string input;
    int hitung = 0, maks = 0;
    cin >> input;
    for(int i=0; i<input.length(); i++){
        if(input[i] == '(' || input[i] == '{' || input[i] == '[' || input[i] == '<'){
            myStack.push(input[i]);
            hitung++;
            if(maks < hitung) maks = hitung;
           }
        else{
            if(myStack.empty()){
                cout << "TIDAK VALID" << endl;
                return 0;
            }else if(myStack.top() == '(' && input[i] == ')') {
                hitung = 0;
                myStack.pop();
               // return true;
            }else if(myStack.top()=='{' && input[i] == '}'){
                hitung = 0;
                myStack.pop();
               // return true;
            }else if(myStack.top()== '[' && input[i] == ']'){
                hitung =0;
                myStack.pop();
               // return true;
            }else if(myStack.top() == '<' && input[i] == '>'){
                hitung = 0;
                myStack.pop();
               // return true;
            }else{
                cout << "TIDAK VALID" << endl;
                return 0;
            }
        }
    }
    cout << maks << endl;
    return 0;
}
