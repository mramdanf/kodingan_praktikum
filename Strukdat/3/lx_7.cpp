#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main(){
    vector<int> bil;
    vector<int>::iterator it;
    double rata_sebelum = 0, rata_sesudah = 0;
    int temp;

    while(cin >> temp, temp != -9){
        bil.push_back(temp);
        rata_sebelum += temp;
    }
    if(!bil.empty())
        rata_sebelum /= (double)bil.size();
    else
        rata_sebelum = -9.99;
    cout << bil.size() << " ";

    int i = 0;
    while(cin >> temp, temp != -9){
        bil.erase(bil.begin()+(temp-i-1));
        i++;
    }
    cout << bil.size() << endl;

    if(!bil.empty()){
        for(it = bil.begin(); it < bil.end(); it++){
            rata_sesudah += *it;
        }
        rata_sesudah /= (double)bil.size();
    } else {
        rata_sesudah = -9.99;
    }

    cout << fixed;
    cout << setprecision(2)
         << rata_sebelum << " "
         << rata_sesudah << endl;


    return 0;
}
