#include <iostream>
#include <list>
#include <string>
using namespace std;

struct node {
	string kode;
	list<float> tinggi;
	float rata;
};
typedef struct node Node;

class DLL : public list<Node> {

	public:
		void print();
};
void DLL::print() {
	list<Node>::iterator ptr;
	for(ptr=this->begin(); ptr!=this->end(); ++ptr) {
		Node item = *ptr;
		cout << item.kode << " : ";
		list<float>::iterator ptr_t;
		for(ptr_t=item.tinggi.begin(); ptr_t!=item.tinggi.end(); ++ptr_t) {
			cout << *ptr_t << "->";
		}
		cout << "NULL" << endl;
	}
}
bool compare_urut_rata (const Node first, const Node second) {
  return first.rata < second.rata;
}
bool compare_urut_kode (const Node first, const Node second) {
  unsigned int i=0;
  while ( (i<first.kode.length()) && (i<second.kode.length()) )
  {
    if ( tolower(first.kode.at(i)) < tolower(second.kode.at(i)) ) return true;
    else if ( tolower(first.kode.at(i)) > tolower(second.kode.at(i)) ) return false;
    ++i;
  }
  return ( first.kode.length() < second.kode.length() );
}
int main() {
	int n,m,i;
	DLL dt;
	string aksi;

	float tinggi,total;

	cin >> n;
	while(n--) {
		Node item;
		cin >> item.kode;
		cin >> m;
		total=0;
		for(i=0;i<m;i++) {
			cin >> tinggi;
			item.tinggi.push_back(tinggi);
			total+=tinggi;
		}
		item.rata = (float)total/m;
		dt.push_back(item);
	}
	cin >> aksi;
	if (aksi.compare("URUTRATA") == 0) dt.sort(compare_urut_rata);
	else if (aksi.compare("URUTKODE") == 0) dt.sort(compare_urut_kode);
	dt.print();
}















