#include <iostream>
#include <list>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
using namespace std;

struct node {
    string nama;
    string asal;
    int fis,mat,stk;
};
typedef struct node Node;
typedef list<Node> ln;
class myList : public list<Node> {
    ln::iterator it;

    public:
        void print() {
            for(it=this->begin();it!=this->end();++it) {
                Node n;
                n = *it;
                cout << n.asal << " ";
            }
            cout << endl;
        }

        void frek() {
            int arr[101]={0};
            vector<pair<string, int> > vt;
            it=this->begin();
            for(it=this->begin();it!=this->end();++it) {
                arr[(*it).mat]++;
                arr[(*it).stk]++;
                arr[(*it).fis]++;

                int i;
                for(i=0; i<vt.size(); i++) {
                    if (vt[i].first.compare((*it).asal) == 0) {
                    	vt[i].second++;
						break;	
					}
                }
                if (i==(vt.size())) vt.push_back(make_pair((*it).asal, 1));
            }

            for(int j=0;j<101;j++) {
                if (arr[j] > 0) {
                	cout << j << ":";
                    for(int k=0;k<arr[j];k++) {
                        cout << "*";
                    }
                    cout << "(" << arr[j] << ")" << endl;
                }
            }

            for(int j=0;j<vt.size();j++) {
            	cout << vt[j].first << ":";
            	for(int k=0;k<vt[j].second;k++) {
            		cout << "*";
				}
				cout << "(" << vt[j].second << ")" << endl;
			}

        }

        void min(string mk) {
            ln dtmin;
            Node n;

            ln::iterator itmin = this->begin();

            if (mk.compare("mat") == 0) {
                for(it=this->begin();it!=this->end();++it) {
                    if ((*it).mat < (*itmin).mat) itmin=it;
                }
                for(it=this->begin();it!=this->end();++it) {
                    if ((*it).mat <= (*itmin).mat) {
                        n = *it;
                        cout << n.nama << "," << n.asal << "," << n.mat << "," << n.stk << "," << n.fis << endl;
                    }
                }
            } else if (mk.compare("stk") == 0) {
                for(it=this->begin();it!=this->end();++it) {
                    if ((*it).stk < (*itmin).stk) itmin=it;
                }
                for(it=this->begin();it!=this->end();++it) {
                    if ((*it).stk <= (*itmin).stk) {
                        n = *it;
                        cout << n.nama << "," << n.asal << "," << n.mat << "," << n.stk << "," << n.fis << endl;
                    }
                }
            } else if (mk.compare("fis") == 0) {
                for(it=this->begin();it!=this->end();++it) {
                    if ((*it).fis < (*itmin).fis) itmin=it;
                }

                for(it=this->begin();it!=this->end();++it) {
                    if ((*it).fis <= (*itmin).fis) {
                        n = *it;
                        cout << n.nama << "," << n.asal << "," << n.mat << "," << n.stk << "," << n.fis << endl;
                    }
                }
            }

        }
};

int main() {
    myList dt;
    char in[256];
    char *p;

    cin >> in;
    p = strtok(in, ",:()");
    while(strcmp(p,"ask") != 0) {

        while(p!=NULL) {
            Node n;
            n.nama = p;
            p = strtok(NULL, ",:()");
            n.asal = p;
            p = strtok(NULL, ",:()");
            n.mat = atoi(p);
            p = strtok(NULL, ",:()");
            n.stk = atoi(p);
            p = strtok(NULL, ",:()");
            n.fis = atoi(p);
            p = strtok(NULL, ",:()");
            dt.push_back(n);
        }

        cin >> in;
        p = strtok(in, ",:()");
    }
    p = strtok(NULL, ",:()");
    if (strcmp(p,"min") == 0) {
        p = strtok(NULL, ",:()");
        dt.min(p);
    } else if (strcmp(p,"hist") == 0) {
        dt.frek();

    }
    return 0;
}
