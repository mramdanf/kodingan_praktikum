#include <iostream>
#include <list>
#include <utility>
using namespace std;

typedef pair <int, int> myPair;

class myList {
    list<myPair > dt;
    list<myPair >::iterator it;
    myPair item;
    int c;

    public:

        myList() {c=1;}

        void push(int val) {
            if (dt.empty()) {
                item.first = val;
                item.second = 1;
                dt.push_back(item);

            } else {

                it=dt.begin();
                while(it!=dt.end() && it->first > val) it++;

                item.first = val;
                item.second = ++c;
                dt.insert(it,item);
            }
        }

        void print() {
            for(it=dt.begin(); it != dt.end(); ++it) {
                cout<< it->second << endl;
            }
        }

};

int main() {
    myList dt;
    int a,i=1;


    cin >> a;
    while(a!=0) {
        dt.push(a);
        cin >> a;
    }
	dt.print();
}
