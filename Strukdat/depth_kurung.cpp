#include <iostream>
#include <stack>
#include <string>

using namespace std;

int cek_kurung(char opening, char closing) {
    if (opening == '[' && closing == ']') return true;
    else if (opening == '{' && closing == '}') return true;
    else if (opening == '(' && closing == ')') return true;
    else if (opening == '<' && closing == '>') return true;
    else return false;

}
int main() {
    stack<char> dt;

    int count=0, max=0, flag=0;
    string input;
    cin>> input;

    for (int i=0;i<input.length();i++) {

        if (input[i]=='('||input[i]== '[' || input[i] == '{' || input[i]=='<') {
                dt.push(input[i]);
                count++;
                if(max<count) max = count;
        } else {
            if(dt.empty()) {
            	flag=1;
            	break;
            }
            if(cek_kurung(dt.top(),input[i])){
                dt.pop();
                count=0;

            } else {
            	flag=1;
            	break;
            }
        }

    }
    if (dt.empty() && !flag) cout<<max<<endl;
    else cout << "TIDAK VALID" << endl;

    return 0;

}
